
from tabulate import tabulate
from datetime import date


class CurrentWeatherData:
    def __init__(self, data):
        self.data = data
        self.location = self.data['location']
        self.current = self.data['current']
        self.condition = self.current['condition']

    def current_output(self, units):
        table = tabulate(
            [
                ['Temperature',
                    f'{self.current[units["temp_key"]]}{units["temp_unit"]}'],
                ['Feels like', self.current[units["feels_like_key"]]],
                ['Conditions', self.condition['text']],
                ['Wind speed',
                    f'{self.current[units["wind_speed_key"]]}{units["wind_unit"]}'],
                ['Humidity', f'{self.current["humidity"]}%'],
                ['Precipitation',
                    f'{self.current[units["precip_key"]]}{units["precip_unit"]}'],
                ['Cloud cover', f'{self.current["cloud"]}%'],
                ['UV index', self.current['uv']]
            ],
            headers=['Current'],
            tablefmt='rounded_outline'
        )
        return table


class ForecastWeatherData:
    def __init__(self, data):
        self.now = CurrentWeatherData(data)
        self.forecast_days = data['forecast']['forecastday']  # list

    def current_output(self, units):
        return self.now.current_output(units)

    def headers(self):
        return [date.fromisoformat(day['date']).strftime('%b %d') for day in self.forecast_days]

    def forecast_output(self, units):
        table = tabulate(
            [
                extend(['Max temp'], [f"{fday['day'][units['max_temp_key']]}{units['temp_unit']}"
                                      for fday in self.forecast_days]),
                extend(['Min temp'], [f"{fday['day'][units['min_temp_key']]}{units['temp_unit']}"
                                      for fday in self.forecast_days]),
                extend(['Avg temp'], [f"{fday['day'][units['avg_temp_key']]}{units['temp_unit']}"
                                      for fday in self.forecast_days]),
                extend(['Max wind'], [f"{fday['day'][units['max_wind_key']]}{units['wind_unit']}"
                                      for fday in self.forecast_days]),
                extend(['Total precip'], [f"{fday['day'][units['total_precip_key']]}{units['precip_unit']}"
                                          for fday in self.forecast_days]),
                extend(['Total snow'], [f"{fday['day']['totalsnow_cm']}cm"
                                        for fday in self.forecast_days]),
                extend(['Avg humidity'], [f"{fday['day']['avghumidity']}%"
                                          for fday in self.forecast_days]),
                extend(['Rain chance'], [f"{fday['day']['daily_chance_of_rain']}%"
                                         for fday in self.forecast_days]),
                extend(['Snow chance'], [f"{fday['day']['daily_chance_of_snow']}%"
                                         for fday in self.forecast_days]),
                extend(['Condition'], [fday['day']['condition']['text']
                                       for fday in self.forecast_days]),
                extend(['UV index'], [fday['day']['uv']
                                      for fday in self.forecast_days]),
            ],
            headers=self.headers(),
            tablefmt='rounded_outline'
        )
        return table


class HourWeatherData:
    def __init__(self, data):
        self.location = data['location']
        self.forecast_days = data['forecast']['forecastday']  # list
        self.hours = [day['hour'] for day in self.forecast_days]

    def headers(self):
        headers = []
        # time isn't exactly in iso format when you include hour and minute
        # so we split it at the space and format each side individually
        # then combine for a nice May 05 12:00 format
        for hour in self.hours:
            time = hour[0]['time'].split(' ')
            day = time[0]
            hour = time[1]
            day = date.fromisoformat(day).strftime('%b %d')
            time = f"{day} {hour}"
            headers.append(time)
        return headers

    def hour_output(self, units):
        table = tabulate(
            [
                extend(['Temperature'], [
                    f"{hour[0][units['temp_key']]}{units['temp_unit']}" for hour in self.hours]),
                extend(['Feels like'], [hour[0][units['feels_like_key']]
                       for hour in self.hours]),
                extend(['Conditions'], [hour[0]['condition']['text']
                       for hour in self.hours]),
                extend(['Wind speed'], [
                    f"{hour[0][units['wind_speed_key']]}{units['wind_unit']}" for hour in self.hours]),
                extend(['Humidity'], [
                       f"{hour[0]['humidity']}%" for hour in self.hours]),
                extend(['Precipitation'], [
                       f"{hour[0][units['precip_key']]}{units['precip_unit']}" for hour in self.hours]),
                extend(['Cloud cover'], [
                       f"{hour[0]['cloud']}% " for hour in self.hours]),
                extend(['UV Index'], [hour[0]['uv'] for hour in self.hours]),
            ],
            headers=self.headers(),
            tablefmt='rounded_outline'
        )
        return table


def extend(ls, values) -> list:
    """Utility method for extending a list AND returning the list, unlike default extend behaviour"""
    ls.extend(values)
    return ls
