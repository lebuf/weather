#! /bin/bash

scriptPath=$(pwd)

# Change to desired bin directory
binPath=/home/$USER/scripts/bin
# +++++++++++++++++++++++++++++++

if [ ! -f $scriptPath/weather-config.json ]; then
    echo "Config file not found, copying template..."
    cp $scriptPath/weather-config.json.template $scriptPath/weather-config.json
fi

if [ -f $scriptPath/cache.json ]; then
    echo "Cache already exists in script folder, overwrite? y/n"
    read answer
    if [[ "$answer" == [yY] ]]; then
        rm $scriptPath/cache.json
        echo "{}" > $scriptPath/cache.json
    else 
        echo "Aborting."
        exit 1
    fi
fi

if [ -f $binPath/weather ]; then
    echo "Script 'weather' already exists in bin folder, overwrite? y/n"
    read answer
    if [[ "$answer" == [yY] ]]; then
        rm $binPath/weather
    else 
        echo "Aborting."
        exit 1
    fi
fi

echo "Adding script link to bin folder..."
echo "$scriptPath/weather.py \"\$@\"" > $binPath/weather

echo "Adding execute privileges..."
chmod +x $binPath/weather

echo "Done!"
