#! /bin/python3

from resolvers import *
from constants import CURRENT, FORECAST
import wclient
from wdata import CurrentWeatherData, ForecastWeatherData, HourWeatherData


def current_output(data, unit):
    cwd = CurrentWeatherData(data)
    output = f'\nCurrent weather for {cwd.location["name"]}, {cwd.location["region"]}, {cwd.location["country"]}\n\n'
    output += cwd.current_output(unit)
    return output


def forecast_output(data, unit, hour):
    if hour:
        hwd = HourWeatherData(data)
        output = f'\nForecast weather at {hour}:00 for {hwd.location["name"]}, {hwd.location["region"]}, {hwd.location["country"]}\n\n'
        output += hwd.hour_output(unit)
    else:
        fwd = ForecastWeatherData(data)
        output = f'\nForecast weather for {fwd.now.location["name"]}, {fwd.now.location["region"]}, {fwd.now.location["country"]}\n\n'
        output += fwd.now.current_output(unit)
        output += '\n\n'
        output += fwd.forecast_output(unit)
    return output


# reset config if flag present
if (args.reset_config):
    if reset_config():
        print('Config reset')
        exit(0)
    else:
        print('Config reset failed')
        exit(1)

# set defaults if flag present
if (args.default):
    if (args.location):
        write_to_config('defaultLocation', args.location)
        print(f'Default location set to {args.location}')
    if (args.units):
        write_to_config('defaultUnits', args.units)
        print(f'Default units set to {config[args.units]}')
    if (args.report):
        write_to_config('defaultReport', args.report)
        print(f'Default report set to {args.report}')
    if (args.days):
        write_to_config('defaultDays', args.days)
        print(f'Default days set to {args.days}')
    if (args.hour):
        write_to_config('defaultHour', args.hour)
        print(f'Default hour set to {args.hour}')


if args.report == CURRENT:
    try:
        data = wclient.get_current(args.location)
        print(current_output(data, unit))
    except Exception as e:
        print(e)
elif args.report == FORECAST:
    try:
        data = wclient.get_forecast(args)
        print(forecast_output(data, unit, args.hour))
    except Exception as e:
        print(e)

print('Powered by https://www.weatherapi.com\n')
