import os
import argparse
import json
import shutil
from dateutil.parser import parse as date_parser
from dotenv import dotenv_values
from constants import *


def os_path(file_name):
    return os.path.join(os.path.dirname(__file__), file_name)


def resolve_env():
    env_path = os_path('.env')
    if not os.path.exists(env_path):
        raise Exception('No .env file found.')
    return dotenv_values(env_path)


def days_type(value):
    if not str.isdigit(value):
        raise argparse.ArgumentTypeError(
            'Forecast days must be integer value.'
        )
    days = int(value)
    if days >= MIN_DAYS and days <= MAX_DAYS:
        return days
    else:
        raise argparse.ArgumentTypeError(
            f'Forecast days must be between {MIN_DAYS} and {MAX_DAYS}'
        )


def date_type(value):
    try:
        return date_parser(value)
    except ValueError:
        raise argparse.ArgumentTypeError(
            'Forecast date must be in format YYYY-MM-DD.'
        )


def hour_type(value):
    if not str.isdigit(value):
        raise argparse.ArgumentTypeError(
            'Forecast hour must be integer value.'
        )
    hour = int(value)
    if hour >= MIN_HOUR and hour <= MAX_HOUR:
        return hour
    else:
        raise argparse.ArgumentTypeError(
            f'Forecast hour must be between {MIN_HOUR} and {MAX_HOUR}')


def resolve_json_file(filename):
    with open(os_path(filename), 'r', encoding='utf-8') as f:
        return json.load(f)


def write_to_json_file(filename, key, value):
    file = resolve_json_file(filename)
    with open(os_path(filename), 'w', encoding='utf-8') as f:
        file[key] = value
        json.dump(file, f, ensure_ascii=False, indent=4)


def resolve_config():
    return resolve_json_file(WEATHER_CONFIG_FILE)


def resolve_cache():
    return resolve_json_file(CACHE_FILE)


def write_to_config(key, value):
    write_to_json_file(WEATHER_CONFIG_FILE, key, value)


def write_to_cache(key, value):
    write_to_json_file(CACHE_FILE, key, value)


def reset_config():
    if os.path.exists(os_path(WEATHER_CONFIG_TEMPLATE)):
        shutil.copyfile(os_path(WEATHER_CONFIG_TEMPLATE),
                        os_path(WEATHER_CONFIG_FILE))
        return True
    else:
        print('No config template file found')
        return False


def resolve_units(config, unit):
    if unit == 'm':
        return config['metricUnits']
    elif unit == 'i':
        return config['imperialUnits']


def resolve_args(default):
    parser = argparse.ArgumentParser(
        prog='weather',
        description='Simple weather forecast utility.'
    )
    parser.add_argument(
        'pos_location',
        metavar='location',
        type=str,
        nargs='?',
        help='Positional argument in place of --location.',
    )
    parser.add_argument(
        'pos_report',
        type=str,
        choices=[CURRENT, FORECAST],
        nargs='?',
        help='Positional argument in place of --report.',
    )
    parser.add_argument(
        'pos_days',
        type=days_type,
        metavar=f'{{{MIN_DAYS}...{MAX_DAYS}}}',
        nargs='?',
        help='Positional argument in place of --days.',
    )
    parser.add_argument(
        '-l',
        '--location',
        type=str,
        default=default['location'],
        help='Location of weather. Can be city name, latitude and longitude, postal/ZIP code.',
    )
    parser.add_argument(
        '-def',
        '--default',
        action='store_true',
        help='Sets defaults, must be used with flags and not positional arguments.',
    )
    parser.add_argument(
        '-r',
        '--report',
        type=str,
        choices=[CURRENT, FORECAST],
        default=default['report'],
        help='Report type to get, default current.',
    )
    parser.add_argument(
        '-u',
        '--units',
        type=str,
        choices=['m', 'i'],
        default=default['units'],
        help='Metric or Imperial units.',
    )
    parser.add_argument(
        '-d',
        '--days',
        type=days_type,
        metavar='{1...14}',
        default=default['days'],
        help='Number of days to display with forecast, must be used with --report forecast.',
    )
    parser.add_argument(
        '-dt',
        '--date',
        type=date_type,
        help='Date in formate YYYY-MM-DD.',
    )
    parser.add_argument(
        '-hr',
        '--hour',
        type=hour_type,
        metavar='{1...23}',
        help=f'Hour of day for forecast in 24 hour format.',
    )
    parser.add_argument(
        '--reset-config',
        action='store_true',
        help='Reset config to template defaults.',
    )

    args = parser.parse_args()
    # set positional arguments if present or named arguments (which contain defaults) otherwise
    args.location = args.pos_location or args.location
    args.report = args.pos_report or args.report
    args.days = args.pos_days or args.days

    return args


# resolve config, defaults and args
config = resolve_config()
default = {
    'location': config['defaultLocation'],
    'units': config['defaultUnits'],
    'report': config['defaultReport'],
    'days': config['defaultDays'],
}
args = resolve_args(default)
unit = resolve_units(config, args.units)
cache = resolve_cache()
