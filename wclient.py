import requests
import urllib.parse
from resolvers import resolve_env, write_to_config, write_to_cache, config, cache
from constants import BASE_URL, FIFTEEN_MINUTES
from datetime import datetime
from time import time

env = resolve_env()
api_key = env['API_KEY']


def current_time_millis():
    return int(round(time()))


def is_cache_valid(key):
    if config[key]:
        return current_time_millis() < config[key] + FIFTEEN_MINUTES
    else:
        return False


def get_current(location):
    if is_cache_valid('currentCacheTime'):
        print(
            f'Using cached current weather data, resets at {datetime.fromtimestamp(config["currentCacheTime"] + FIFTEEN_MINUTES)}'
        )
        return cache['current']
    else:
        params = urllib.parse.urlencode(
            {
                'q': location,
                'key': api_key
            }
        )
        url = f'{BASE_URL}/current.json?{params}'

        try:
            response = requests.get(url)
            if (response.status_code == 200):
                write_to_config('currentCacheTime', current_time_millis())
                write_to_cache('current', response.json())
                return response.json()
            else:
                raise Exception(
                    f'Unable to fetch data from WeatherAPI, status code: {response.status_code} message: {response.json()["error"]["message"]}'
                )
        finally:
            response.close()


def get_forecast(args):
    params = urllib.parse.urlencode(
        {
            'q': args.location,
            'days': args.days,
            'dt': args.date,
            'hour': args.hour,
            'key': api_key
        }
    )
    url = f'{BASE_URL}/forecast.json?{params}'

    try:
        response = requests.get(url)
        if (response.status_code == 200):
            write_to_config('forecastCacheTime', current_time_millis())
            write_to_cache('forecast', response.json())
            return response.json()
        else:
            raise Exception(
                f'Unable to fetch data from WeatherAPI, status code: {response.status_code} message: {response.json()["error"]["message"]}'
            )
    finally:
        response.close()
